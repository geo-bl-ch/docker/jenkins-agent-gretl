FROM openshift/jenkins-slave-base-centos7

ARG gradle_version=5.6.1

# Install gradle
ENV GRADLE_VERSION=${gradle_version}
RUN wget https://downloads.gradle-dn.com/distributions/gradle-${gradle_version}-bin.zip && \
    mkdir /opt/gradle && \
    unzip -d /opt/gradle gradle-${gradle_version}-bin.zip && \
    rm -f gradle-${gradle_version}-bin.zip && \
    ln -s /opt/gradle/gradle-${gradle_version}/bin/gradle /usr/local/bin/ && \
    useradd -u 1001 -g root -m -d /data -s /bin/bash gretl && \
    chgrp root /data && \
    chmod g=u /data

# Install PostGIS
RUN wget https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm && \
    yum -y install pgdg-redhat-repo-latest.noarch.rpm epel-release && \
    rm -f pgdg-redhat-repo-latest.noarch.rpm && \
    yum -y install postgresql10 postgresql10-server postgis25_10 && \
    yum clean all && \
    chgrp -R 0 /var/run/postgresql && \
    chmod -R g=u /var/run/postgresql

# Initialize database
ENV PGHOST="localhost" \
    PGUSER="postgres" \
    PGPASSWORD="postgres" \
    PGDATA="/pg_data"
RUN mkdir -p ${PGDATA} && \
    echo "postgres" > ${PGDATA}/password && \
    chown -R gretl ${PGDATA} && \
    chgrp -R root ${PGDATA} && \
    chmod -R g=u ${PGDATA}

ADD bin/* /usr/local/bin/

WORKDIR /data

USER 1001
